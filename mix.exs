defmodule Mastobot.MixProject do
  use Mix.Project

  def project do
    [
      app: :mastobot,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Mastobot.Application, []}
    ]
  end

  defp deps do
    [
      {:tesla, "~> 1.3"},
      {:mint, "~> 1.1"},
      {:castore, "~> 0.1.7"},
      {:jason, "~> 1.2"},
      {:feeder_ex, "~> 1.1"},
      {:fast_sanitize, "~> 0.2.1"},
      {:floki, "~> 0.28.0"},
      {:uuid, "~> 1.1"},
      {:toml, "~> 0.6.2"},
      {:yaml_elixir, "~> 2.5"}
    ]
  end
end
