defmodule Mastobot.Strategies do
  def apply([], _), do: []

  def apply(buffer, :all), do: buffer

  def apply(_buffer, :discard) do
    nil
  end

  def apply(buffer, :last) do
    buffer
    |> List.last()
    |> List.wrap()
  end

  def apply(buffer, amount) when is_integer(amount) do
    buffer
    |> Enum.reverse()
    |> Enum.take(amount)
    |> Enum.reverse()
  end
end
