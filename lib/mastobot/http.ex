defmodule Mastobot.Http do
  defstruct [:token, :url, type: :json]

  def client(%__MODULE__{} = base) do
    [
      get_parser(base),
      {Tesla.Middleware.BaseUrl, base.url},
      get_header(base)
    ]
    |> Tesla.client(Tesla.Adapter.Mint)
  end

  defp get_parser(%__MODULE__{type: :json}), do: Tesla.Middleware.JSON
  defp get_parser(%__MODULE__{type: :xml}), do: Mastobot.Http.Xml
  defp get_parser(%__MODULE__{type: :html}), do: Mastobot.Http.Html

  defp get_header(%__MODULE__{type: :json, token: nil}) do
    {Tesla.Middleware.Headers,
     [{"Content-Type", "application/json"}, {"Accept", "Application/json; Charset=utf-8"}]}
  end

  defp get_header(%__MODULE__{type: :json, token: token}) do
    {Tesla.Middleware.Headers,
     [
       {"Content-Type", "application/json"},
       {"Accept", "application/json; charset=utf-8"},
       {"Authorization", "Bearer #{token}"}
     ]}
  end

  defp get_header(%__MODULE__{type: :xml}) do
    {Tesla.Middleware.Headers,
     [
       {"Content-Type", "application/rss+xml"},
       {"Accept", "application/rss+xml; charset=utf-8, application/atom+xml; charset=utf-8"}
     ]}
  end

  defp get_header(%__MODULE__{type: :html}) do
    {Tesla.Middleware.Headers,
     [
       {"Contet-Type", "text/html"},
       {"Accept", "text/html; charset=utf-8"}
     ]}
  end
end
