defmodule Mastobot.Source do
  use GenServer

  def child_spec(opts) do
    %{
      id: opts[:name],
      start: {__MODULE__, :start_link, [opts]},
      type: :worker
    }
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: opts[:name])
  end

  def init(opts) do
    with bot when not is_nil(bot) <- opts[:bot],
         pid when is_pid(pid) <- Process.whereis(bot) do
      sync_feed()
      {:ok, nil, {:continue, opts}}
    else
      _ ->
        {:error, [reason: :no_bot, feed: opts[:name]]}
    end
  end

  def handle_continue(opts, nil) do
    handler = get_handler(opts[:handler])

    state =
      opts
      |> handler.prepare_state()
      |> handler.sync_feed()
      |> handler.sync_buffer()

    maybe_sync_buffer(state[:buffer])

    {:noreply, state}
  end

  def handle_info(:sync_feed, state) do
    sync_feed()
    handler = state[:handler]

    state =
      state
      |> handler.sync_feed()
      |> handler.sync_buffer()

    maybe_sync_buffer(state[:buffer])

    {:noreply, state}
  end

  def handle_info(:sync_buffer, state) do
    state = state[:handler].sync_buffer(state)
    maybe_sync_buffer(state[:buffer])
    {:noreply, state}
  end

  defp get_handler(:feed), do: Mastobot.Source.Feed
  defp get_handler(:telegram), do: Mastobot.Source.Telegram

  defp maybe_sync_buffer([]), do: :ok
  defp maybe_sync_buffer(_), do: Process.send_after(self(), :sync_buffer, 600_000)

  defp sync_feed, do: Process.send_after(self(), :sync_feed, 1_800_000)
end
