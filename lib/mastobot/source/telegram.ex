defmodule Mastobot.Source.Telegram do
  require Logger

  def prepare_state(opts) do
    client = build_base_client(opts[:url])
    current = Mastobot.HistoryFile.get(opts[:name])

    %{
      name: opts[:name],
      max_characters: opts[:max_characters],
      bot: opts[:bot],
      handler: __MODULE__,
      url: opts[:url],
      client: client,
      strategy: opts[:strategy],
      current: current,
      buffer: nil
    }
  end

  def sync_feed(%{current: nil} = state) do
    Logger.info("sync feed #{state[:name]}")
    {:ok, %{body: body}} = Tesla.get(state[:client], "")

    %{state | buffer: list_all(body)}
  end

  def sync_feed(%{current: current} = state) do
    Logger.info("sync feed #{state[:name]}")
    {:ok, %{body: body}} = Tesla.get(state[:client], "")

    buffer =
      body
      |> list_all()
      |> Enum.reverse()
      |> Enum.take_while(&(elem(&1, 0) != current))
      |> Enum.reverse()

    %{state | buffer: buffer}
  end

  def sync_buffer(%{current: nil} = state) do
    Logger.info("sync buffer #{state[:name]}")

    case Mastobot.Strategies.apply(state[:buffer], state[:strategy]) do
      [] ->
        %{state | buffer: []}

      [current | rest] ->
        publish(state, current)
        update_current(state[:name], current)

        %{state | buffer: rest, current: elem(current, 0)}

      nil ->
        last = List.last(state[:buffer])
        update_current(state[:name], last)

        %{state | buffer: [], current: last.link}
    end
  end

  def sync_buffer(%{buffer: []} = state) do
    Logger.info("sync buffer #{state[:name]}")
    state
  end

  def sync_buffer(%{buffer: [current | rest]} = state) do
    Logger.info("sync buffer #{state[:name]}")
    publish(state, current)

    update_current(state[:name], current)

    %{state | buffer: rest, current: elem(current, 0)}
  end

  defp publish(state, {id, text}) do
    path =
      state
      |> Map.get(:url)
      |> String.split("/")
      |> List.last()

    link = "https://t.me/#{path}/#{id}"
    status = build_status(text, link, state[:max_characters])

    Mastobot.Mastodon.Bot.post(state[:bot], status)
  end

  defp update_current(name, {id, _text}) do
    Mastobot.HistoryFile.log(name, id)
  end

  defp list_all(html) do
    Enum.zip(list_ids(html), list_messages(html))
  end

  defp list_ids(html) do
    html
    |> Floki.find(".tgme_widget_message")
    |> Enum.map(&Floki.attribute(&1, "data-post"))
    |> List.flatten()
    |> Enum.map(&String.split(&1, "/"))
    |> Enum.map(&List.last/1)
    |> Enum.map(&String.to_integer/1)
  end

  defp list_messages(html) do
    html
    |> Floki.find(".tgme_widget_message_text")
    |> Enum.map(&Floki.raw_html/1)
    |> Enum.map(&sanitize_message/1)
  end

  defp sanitize_message(message) do
    message
    |> String.split("<br/>")
    |> Enum.reject(&(&1 == ""))
    |> Enum.join("\n")
    |> FastSanitize.basic_html()
    |> elem(1)
    |> FastSanitize.strip_tags()
    |> elem(1)
    |> String.trim()
  end

  defp build_status(text, link, max) do
    text_length = max - String.length(link) - 7
    new_text = String.slice(text, 0, text_length)

    if String.length(text) <= text_length do
      "#{text}\n#{link}"
    else
      "#{new_text}[...]\n#{link}"
    end
  end

  defp build_base_client(url) do
    Mastobot.Http.client(%Mastobot.Http{url: url, type: :html})
  end
end
