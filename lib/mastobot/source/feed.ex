defmodule Mastobot.Source.Feed do
  require Logger

  def prepare_state(opts) do
    current = Mastobot.HistoryFile.get(opts[:name])
    client = build_base_client(opts[:url])

    %{
      name: opts[:name],
      max_characters: opts[:max_characters],
      fields: opts[:fields],
      bot: opts[:bot],
      handler: __MODULE__,
      client: client,
      strategy: opts[:strategy],
      current: current,
      buffer: nil
    }
  end

  def sync_feed(%{current: nil} = state) do
    Logger.info("sync feed #{state[:name]}")
    {:ok, %{body: feed}} = Tesla.get(state[:client], "")

    %{state | buffer: Enum.reverse(feed.entries)}
  end

  def sync_feed(%{current: current} = state) do
    Logger.info("sync feed #{state[:name]}")
    {:ok, %{body: feed}} = Tesla.get(state[:client], "")

    buffer =
      feed.entries
      |> Enum.take_while(&(&1.link != current))
      |> Enum.reverse()

    %{state | buffer: buffer}
  end

  def sync_buffer(%{current: nil} = state) do
    Logger.info("sync buffer #{state[:name]}")

    case Mastobot.Strategies.apply(state[:buffer], state[:strategy]) do
      [] ->
        %{state | buffer: []}

      [current | rest] ->
        publish(state, current)
        update_current(state[:name], current)

        %{state | buffer: rest, current: current.link}

      nil ->
        last = List.last(state[:buffer])
        update_current(state[:name], last)

        %{state | buffer: [], current: last.link}
    end
  end

  def sync_buffer(%{buffer: []} = state) do
    Logger.info("sync buffer #{state[:name]}")
    state
  end

  def sync_buffer(%{buffer: [current | rest]} = state) do
    Logger.info("sync buffer #{state[:name]}")
    publish(state, current)
    update_current(state[:name], current)

    %{state | buffer: rest, current: current.link}
  end

  defp publish(state, current) do
    text =
      state
      |> Map.get(:fields)
      |> Enum.map(&Map.get(current, &1))
      |> Enum.map(&text_sanitize/1)
      |> Enum.join("\n")

    status = build_status(text, current.link, state[:max_characters])

    Mastobot.Mastodon.Bot.post(state[:bot], status)
  end

  defp update_current(name, current) do
    Mastobot.HistoryFile.log(name, current.link)
  end

  defp build_status(text, link, max) do
    text_length = max - String.length(link) - 7
    new_text = String.slice(text, 0, text_length)

    if String.length(text) <= text_length do
      "#{text}\n#{link}"
    else
      "#{new_text}[...]\n#{link}"
    end
  end

  defp text_sanitize(text) do
    {:ok, text} =
      text
      |> String.split("\n")
      |> Enum.map(&String.trim/1)
      |> Enum.reject(&String.contains?(&1, "appeared first on"))
      |> Enum.join("\n")
      |> FastSanitize.strip_tags()

    text
  end

  defp build_base_client(url) do
    Mastobot.Http.client(%Mastobot.Http{url: url, type: :xml})
  end
end
