defmodule Mastobot.HistoryFile do
  use GenServer
  require Logger

  def child_spec(_) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [:ok]},
      type: :worker
    }
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def log(feed, id) do
    GenServer.call(__MODULE__, {:log, feed, id}, :infinity)
  end

  def get(feed) do
    GenServer.call(__MODULE__, {:get, feed}, :infinity)
  end

  def init(:ok) do
    sync()

    file =
      :mastobot
      |> :code.priv_dir()
      |> Path.join("/history/ets_file.db")
      |> String.to_charlist()

    table =
      with true <- File.exists?(file),
           {:ok, table} <- :ets.file2tab(file, verify: true) do
        table
      else
        _ -> :ets.new(:history_file, [:set, :private])
      end

    {:ok, %{file: file, table: table}}
  end

  def handle_info(:sync, state) do
    sync()
    Logger.info("writing file #{state[:file]}")
    :ets.tab2file(state[:table], state[:file], sync: true, extendend_info: [:md5sum])

    {:noreply, state}
  end

  def handle_call({:log, feed, id}, _from, state) do
    Logger.info("logging #{feed}, #{id}")
    :ets.insert(state[:table], {feed, id})
    {:reply, :ok, state}
  end

  def handle_call({:get, feed}, _from, state) do
    response =
      case :ets.lookup(state[:table], feed) do
        [{^feed, value}] -> value
        _ -> nil
      end

    {:reply, response, state}
  end

  def terminate(_reason, state) do
    Logger.info("writing file #{state[:file]}")
    :ets.tab2file(state[:table], state[:file], sync: true, extendend_info: [:md5sum])
    :normal
  end

  defp sync, do: Process.send_after(self(), :sync, 3600_000)
end
