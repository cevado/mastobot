defmodule Mastobot.Mastodon.Client do
  defstruct [:url, :token, :vapid_key, :id, :secret]

  alias Mastobot.Http

  @redirect_uri "urn:ietf:wg:oauth:2.0:oob"
  @scope "read write"
  @name "mastobot"
  @website "https://gitlab.com/cevado/mastobot"
  @grant_type "password"

  def create_app(%__MODULE__{id: nil} = client) do
    response =
      client
      |> build_client()
      |> Tesla.post("/api/v1/apps", %{
        client_name: @name,
        redirect_uris: @redirect_uri,
        scopes: @scope,
        website: @website
      })

    with {:ok, %{body: body, status: 200}} <- response do
      {:ok,
       %{
         client
         | id: body["client_id"],
           secret: body["client_secret"],
           vapid_key: body["vapid_key"]
       }}
    end
  end

  def create_app(%__MODULE__{} = client), do: {:ok, client}

  def authorize(%__MODULE__{id: nil} = client, _params), do: {:error, :create_app}

  def authorize(%__MODULE__{} = client, params) do
    response =
      client
      |> build_client()
      |> Tesla.post("/oauth/token", %{
        redirect_uri: @redirect_uri,
        scope: @scope,
        grant_type: @grant_type,
        client_id: client.id,
        client_secret: client.secret,
        username: Base.decode64!(params[:username]),
        password: Base.decode64!(params[:password])
      })

    with {:ok, %{body: body, status: 200}} <- response do
      {:ok, %{client | token: body["access_token"]}}
    end
  end

  def post_status(%__MODULE__{token: nil} = client, _params), do: {:error, :authorize}

  def post_status(%__MODULE__{} = client, params) do
    response =
      client
      |> build_client()
      |> Tesla.post("/api/v1/statuses", params)

    with {:ok, %{status: 200}} <- response do
      :ok
    end
  end

  defp build_client(%__MODULE__{} = client) do
    %Http{token: client.token, url: client.url, type: :json}
    |> Http.client()
  end
end
