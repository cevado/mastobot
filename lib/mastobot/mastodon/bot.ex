defmodule Mastobot.Mastodon.Bot do
  use GenServer

  alias Mastobot.Mastodon.Client

  def child_spec(opts) do
    %{
      id: opts[:name],
      start: {__MODULE__, :start_link, [opts]},
      type: :worker
    }
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: opts[:name])
  end

  def post(name, status) do
    GenServer.call(name, {:status, status}, :infinity)
  end

  def init(opts) do
    sync()
    {:ok, client} = Client.create_app(%Client{url: opts[:instance]})

    {:ok, client} =
      Client.authorize(client, %{username: opts[:username], password: opts[:password]})

    {:ok, %{client: client, buffer: []}}
  end

  def handle_info(:sync, state) do
    sync()
    {:noreply, post_status(state)}
  end

  def handle_call({:status, status}, _from, state) do
    {:reply, :ok, %{state | buffer: [status | state[:buffer]]}}
  end

  defp post_status(%{buffer: []} = state), do: state

  defp post_status(state) do
    [status | rest] = Enum.reverse(state[:buffer])
    Client.post_status(state[:client], %{status: status, visibility: "public"})

    %{state | buffer: Enum.reverse(rest)}
  end

  defp sync(), do: Process.send_after(self(), :sync, 15_000)
end
