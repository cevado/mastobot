defmodule Mastobot.Http.Xml do
  @behaviour Tesla.Middleware

  def call(env, next, _optins) do
    env
    |> Tesla.run(next)
    |> parse()
  end

  defp parse({:ok, %Tesla.Env{body: body} = env}) do
    {:ok, parsed, _rest} = FeederEx.parse(body)
    {:ok, %{env | body: parsed}}
  end
end
