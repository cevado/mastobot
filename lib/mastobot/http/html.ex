defmodule Mastobot.Http.Html do
  @behaviour Tesla.Middleware

  def call(env, next, _optins) do
    env
    |> Tesla.run(next)
    |> parse()
  end

  defp parse({:ok, %Tesla.Env{body: body} = env}) do
    {:ok, parsed} = Floki.parse_document(body)
    {:ok, %{env | body: parsed}}
  end
end
