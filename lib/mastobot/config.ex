defmodule Mastobot.Config do
  def load do
    "MASTOBOT_CONFIG"
    |> System.get_env()
    |> identify()
    |> load()
    |> prepare()
  end

  defp identify(file) do
    term =
      file
      |> String.split(".")
      |> List.last()

    {term, file}
  end

  defp load({"yaml", file}) do
    case YamlElixir.read_from_file(file) do
      {:ok, parsed} -> parsed
      {:error, _error} -> file_error()
    end
  end

  defp load({"json", file}) do
    case json_load(file) do
      {:ok, parsed} -> parsed
      {:error, _error} -> file_error()
    end
  end

  defp load({"toml", file}) do
    case Toml.decode_file(file) do
      {:ok, parsed} -> parsed
      {:error, _error} -> file_error()
    end
  end

  defp load({_, file}) do
    file_error()
  end

  defp json_load(file) do
    file
    |> File.read!()
    |> Jason.decode()
  end

  defp file_error do
    require Logger
    Logger.error("[mastobot] - invalid format config file: #{file}")
    System.stop(0)
  end

  defp prepare(config) do
    sources = Enum.map(config["sources"], &prepare_source/1)
    bots = Enum.map(config["bots"], &prepare_bot/1)

    with {true, _} <- {verify_bots(bots), :bots},
         {true, _} <- {Enum.all?(&valid_source?(bots, &1)), :sources},
         db_file when not is_nil(db_file) <- config["db_file"] do
      {sources, bots, db_file}
    else
      {_, :bots} ->
        stop(:bots)

      {_, :sources} ->
        stop(:sources)

      nil ->
        stop(:db_file)
    end
  end

  @bot_keys ~w(name instance username password)
  defp prepare_bot(bot) do
    bot
    |> Enum.filter(&(elem(&1, 0) in @bot_keys))
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
    |> Enum.into(%{})
    |> Map.update(:name, nil, &String.to_atom/1)
  end

  @source_keys ~w(name url max_characters fields strategy handler bot)
  defp prepare_source(source) do
    source
    |> Enum.filter(&(elem(&1, 0) in @source_keys))
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
    |> Enum.into(%{})
    |> Map.update(:name, nil, &String.to_atom/1)
    |> Map.update(:strategy, nil, &String.to_atom/1)
    |> Map.update(:bot, nil, &String.to_atom/1)
  end

  defp stop(reason) do
    require Logger
    Logger.error("[mastobot] - invalid config please verify your #{reason} section.")
  end
end
