defmodule Mastobot.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    # bots =
    #   :mastobot
    #   |> Application.fetch_env!(:bots)
    #   |> Enum.map(&{Mastobot.Mastodon.Bot, &1})
    #
    # feeds =
    #   :mastobot
    #   |> Application.fetch_env!(:feeds)
    #   |> Enum.map(&{Mastobot.Feed, &1})

    # children = List.flatten([Mastobot.HistoryFile, bots, feeds])
    children = []
    opts = [strategy: :one_for_one, name: Mastobot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
