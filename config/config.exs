import Config

config :mastobot,
  bots: [
    [
      name: "obrigahistoria",
      instance: "https://botsin.space",
      username: "ZmxhdmlvLm12K29icmlnYWhpc3RvcmlhQGdtYWlsLmNvbQ==",
      password: "NGQ5OGE0N2YtZmU3Mi00ZTRjLTkwZWUtMjE1YmY4ZThhY2Vl"
    ]
  ],
  feeds: [
    [
      name: "obrigahistoria+podcast",
      url: "https://leituraobrigahistoria.com/category/podcast/feed/",
      bot: "obrigahistoria"
    ],
    [
      name: "obrigahistoria+texto",
      url: "https://leituraobrigahistoria.com/category/textos/feed/",
      bot: "obrigahistoria"
    ],
    [
      name: "obrigahistoria+youtube",
      url: "https://www.youtube.com/feeds/videos.xml?channel_id=UCtMjnvODdK1Gwy8psW3dzrg",
      bot: "obrigahistoria"
    ]
  ]
