* Mastobot
A configurable small elixir application to post status based on RSSFeeds
** About
The objective of this project is to have a small footprint, easy to use and configurable Mastodon Bot that just publishes status based on different sources.
This is a full application, you don't need to know how to code to use it.
The idea is to configure your bots and sources of information for the bots and the application will do everything else.

** Project Planning
+ [ ] functionalities
  + [x] post to mastodon
  + [x] consume rssfeeds
  + [ ] add task to mask usernames and passwords
  + [ ] add task to generate random passwords
  + [ ] configurable via yaml file
  + [x] consume telegram channels
  + [ ] configurable via toml file
  + [ ] consume instagram pages
  + [ ] consume facebook pages
+ [ ] codebase
  + [ ] prepare build
  + [ ] fully tested
  + [ ] use releases
+ [ ] distribution
  + [ ] provide amd64 linux artifacts
  + [ ] provide x86 linux artifacts
  + [ ] provide arm linux artifacts
  + [ ] provide windows artifacts
+ [ ] documentation
  + [ ] document codebase
  + [ ] document configuration
  + [ ] document usage
+ [ ] community
  + [ ] add license
  + [ ] contribution guide
  + [ ] build badge
  + [ ] coverage badge
  + [ ] code quality badge
  + [ ] doc quality badge
